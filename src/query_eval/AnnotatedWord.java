package query_eval;

import java.util.Map;

public class AnnotatedWord {
	private String word;
	private Double idf;
	private Integer fileNo;
	private Integer byteNo;
	private Map<Integer,AnnotatedDocument> documents;
	
	public AnnotatedWord(String word,Integer fileNo, double idf, Integer byteNo){
		this.word=word;
		this.idf=idf;
		this.fileNo=fileNo;
		this.byteNo=byteNo;
	}
	
	public String toString() {
		return "Word: "+word+" idf: "+idf+" fileNo: "+fileNo+" Pointer: "+byteNo;
	}
	
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public Double getidf() {
		return idf;
	}
	public void setIdf(Double idf) {
		this.idf = idf;
	}
	public Integer getByteNo() {
		return byteNo;
	}
	public void setByteNo(Integer byteNo) {
		this.byteNo = byteNo;
	}

	public Map<Integer,AnnotatedDocument> getDocuments() {
		return documents;
	}

	public void setDocuments(Map<Integer,AnnotatedDocument> documents) {
		this.documents = documents;
	}

	public Integer getFileNo() {
		return fileNo;
	}

	public void setFileNo(Integer fileNo) {
		this.fileNo = fileNo;
	}
}
