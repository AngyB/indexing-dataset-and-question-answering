package query_eval;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

import opennlp.tools.stemmer.PorterStemmer;
import opennlp.tools.stemmer.Stemmer;

abstract public class Parser {
	public static String INDEX_PATH = "./CollectionIndex";
	public static String VOCABULARY_FILENAME = INDEX_PATH + "/VocabularyFile.txt";
	public static String POSTING_FILENAME = INDEX_PATH + "/PostingFile.txt";
	public static String DOCUMENTS_FILENAME = INDEX_PATH + "/DocumentsFile.txt";
	
	private static RandomAccessFile PostingFile = null;
	private static RandomAccessFile DocumentsFile = null;
	
	public static void Init() throws IOException {
		PostingFile = new RandomAccessFile(POSTING_FILENAME, "r");
		DocumentsFile = new RandomAccessFile(DOCUMENTS_FILENAME, "r");
	}

	public static Map<String, AnnotatedWord> getVocabulary(Map<String, AnnotatedWord> words) throws IOException {

//		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), StandardCharsets.UTF_8));
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(VOCABULARY_FILENAME), StandardCharsets.UTF_8));	
		
		String sentence = in.readLine();
		
		String delimiter = "\s\t";
		
		PorterStemmer stemmer= new PorterStemmer();
//		Stemmer.Initialize();
		
		while(sentence!=null) {
			
			StringTokenizer tokenizer=new StringTokenizer(sentence, delimiter);
			
			String token = tokenizer.nextToken();
			String fileno = tokenizer.nextToken();
			String idf = tokenizer.nextToken();
			String byteNo = tokenizer.nextToken();
			
			AnnotatedWord word = new AnnotatedWord(token, Integer.parseInt(fileno), Double.parseDouble(idf), Integer.parseInt(byteNo));
			
			words.put(token, word);
			
			sentence = in.readLine();
		}
		in.close();
		
		return words;
		
	}
	
	public static List<String> createStopwordsList(String filepath, List <String> stopwords) throws IOException{
		
		String line="";
		FileReader file = new FileReader(filepath);
		
		@SuppressWarnings("resource")
		BufferedReader reader= new BufferedReader(file);
		while(line!=null) {
			line=reader.readLine();
			if(line!=null) {
				line.replaceAll("\\s","");
				stopwords.add(line);
			}
		}
		
		return stopwords;
	}

	public static List<Entry<String, Double>>AnswerQuestion(String query, Map<String, AnnotatedWord> words) throws IOException{
//		List<Entry<String, Double>>relatedFiles=new ArrayList<Entry<String, Double>>();
//		String delimiter = "\s\t\n\r\f. , ? # & ( ) ; ~ { }+ - / \\  | = : !\" \' $ % _ * [ ] @ ^ ` 0 1 2 3 4 5 6 7 8 9";
//		String lineDelimiter="\t\n";
		String[] tokens = query.split("\\s+"); // split on whitespace
		Integer maxFreq=0;
		
		Map <AnnotatedWord, Integer> queryMap=new HashMap<AnnotatedWord, Integer>();
		
		Map<Integer, AnnotatedDocument> documents=new TreeMap<Integer, AnnotatedDocument>();
		
//		Stemmer.Initialize();
		for (String currentToken : tokens) {
			
//			String currentToken = tokenizer.nextToken();
//			currentToken=Stemmer.Stem(currentToken);
			
			if(words.containsKey(currentToken)) {
				AnnotatedWord word = words.get(currentToken);
				
				if(!queryMap.containsKey(word)) {
					queryMap.put(word,0);
				}
				
				queryMap.put(word,queryMap.get(word)+1);
				if(maxFreq<queryMap.get(word)) {
					maxFreq=queryMap.get(word);
				}
				
//				System.out.println(currentToken);
				
//				System.out.println(word);
				
				List<String> lines=getPostingList(word.getByteNo(), word.getFileNo());
				
				for(int i=0;i<lines.size();i++) {
				
//					System.out.println(lines.get(i));
//					StringTokenizer lineTokenizer = new StringTokenizer(lines.get(i), lineDelimiter);
					String[] fields = lines.get(i).split("\\s+");
					
					AnnotatedDocument tmp=new AnnotatedDocument();
					tmp.setFileno(Integer.parseInt(fields[0]));
					tmp.setTf(Double.parseDouble(fields[1]));
					tmp.setByteNo(Integer.parseInt(fields[2]));
					
					documents.put(tmp.getByteNo(), tmp);
					
//					System.out.println(token);
				}
				
				getDocumentInfo(documents);
				word.setDocuments(documents);
				documents=new TreeMap<Integer, AnnotatedDocument>();
//				for ( Entry<Integer, AnnotatedDocument> entry : documents.entrySet()) {
//					System.out.println(entry.getValue().toString());
//				}
			}
		}
		
		List<Entry<String, Double>>relatedFiles = null;
		
		if(queryMap.size()!=0) {
			relatedFiles=CosSim(queryMap,maxFreq);
		}
		
		
		
		return relatedFiles;
	}

	public static List<Entry<String, Double>> CosSim(Map <AnnotatedWord, Integer>queryMap, Integer maxFreq) {

		HashMap <String ,Double> sim= new HashMap<String, Double>();
		Double sum=0.0 ;
		for ( Entry<AnnotatedWord, Integer> entry : queryMap.entrySet()) {
			AnnotatedWord qWord=entry.getKey();
			for ( Entry<Integer, AnnotatedDocument> entryDocument : qWord.getDocuments().entrySet()) {
//				System.out.println(entry.getValue().toString());
				
				String filename= entryDocument.getValue().getFilename();
				if(!sim.containsKey(filename)) {
					sim.put(filename, 0.0);
				}
				sum=sum+qWord.getidf()*(entry.getValue()/maxFreq)*qWord.getidf()*(entry.getValue()/maxFreq);
				sim.put(filename,sim.get(filename)+ ((entryDocument.getValue().getTf()*qWord.getidf())*(entry.getValue()/maxFreq)*qWord.getidf())/ entryDocument.getValue().getNorm());
			}
			
		}
		
		sum=Math.sqrt(sum);
		for ( Entry<String, Double> entry : sim.entrySet()) {
			entry.setValue(entry.getValue()*(1/sum));
//			System.out.println(entry.getKey()+" "+entry.getValue());
		}
		
		//===========Sort answer============= 
		Set <Entry<String, Double>> hashmap = sim.entrySet();
		
		List<Entry<String, Double>> list =new ArrayList<>(hashmap);
		
		Collections.sort(list, new Comparator<Entry<String, Double>>(){
		
		public int compare(Entry<String , Double>o1, Entry<String , Double>o2) {
			return (o2.getValue()).compareTo(o1.getValue());
			}
		});
		
//		list.forEach(s->{
//			System.out.println(s.getKey()+"\t"+s.getValue());
//		});
		
		
		return list;
	}

	public static void getDocumentInfo(Map<Integer, AnnotatedDocument> documents) throws IOException {		
		Integer Byteno=0;
		
		for ( Entry<Integer, AnnotatedDocument> entry : documents.entrySet()) {
			Byteno=entry.getKey();
			if (Byteno != DocumentsFile.getFilePointer())
				DocumentsFile.seek(Byteno);
			String tmp=DocumentsFile.readLine();
//			System.out.println(tmp);
			
			StringTokenizer lineTokenizer = new StringTokenizer(tmp, "\t");
			lineTokenizer.nextToken();
			entry.getValue().setFilename(lineTokenizer.nextToken());
			entry.getValue().setNorm(Double.parseDouble(lineTokenizer.nextToken()));
		}		
	}
	
	public static List<String> getPostingList(int ByteNo, int FilesNo) throws IOException {
		List<String> str=new ArrayList<String>();

		PostingFile.seek(ByteNo);
		
		for(int i=0;i<FilesNo;i++) {
			String tmp=PostingFile.readLine();
			
			str.add(tmp);
		}
		
		return str;
	}
	
	
//	public static String getQuery(String filePath, Integer topicNum) throws Exception {
////		ArrayList<Topic> topics = TopicsReader.readTopics(filePath);
//
//		System.out.println(topics.get(topicNum-1).getNumber());
//		System.out.println(topics.get(topicNum-1).getType());
//		System.out.println(topics.get(topicNum-1).getSummary());
//		System.out.println(topics.get(topicNum-1).getDescription());
//		System.out.println("---------");
//			
////		return topics.get(topicNum-1).getSummary();
//	}
		
	
}
