package query_eval;

public class AnnotatedDocument {
	private String filename;
	private Integer fileno;
	private Double Norm;
	private Double tf;
	private Integer ByteNo;
	
	public String toString() {
		String str="";
		
		str=str+"filename: "+filename+" fileno: "+fileno+" Norm: "+Norm+" tf: "+tf+" tf: "+ByteNo+"\n";
		
		return str;
	}
	
	public Integer getFileno() {
		return fileno;
	}
	public void setFileno(Integer fileno) {
		this.fileno = fileno;
	}
	public Double getNorm() {
		return Norm;
	}
	public void setNorm(double d) {
		Norm = d;
	}
	public Double getTf() {
		return tf;
	}
	public void setTf(Double tf) {
		this.tf = tf;
	}
	public Integer getByteNo() {
		return ByteNo;
	}
	public void setByteNo(Integer byteNo) {
		ByteNo = byteNo;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	

}
