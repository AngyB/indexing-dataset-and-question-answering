package query_eval;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;



public class query_eval {

	public static void main(String[] args) throws Exception {
		Map<String, AnnotatedWord>words = new TreeMap<String, AnnotatedWord>();
		String path ="./files";
		//================parse Vocabulary file=================
		Parser.Init();
		words=Parser.getVocabulary(words);
		
//		for ( Entry<String, AnnotatedWord> entry : words.entrySet()) {
//			System.out.println(entry.getValue().toString()+"\n");
//		}
		
		System.out.println("give number of topic:");
		
		Scanner in = new Scanner(System.in);
		
		String query="abduct a cat \n";
		
//		query=Parser.getQuery(path+"/topics.xml",in.nextInt());
//		in.close();
		
		List<Entry<String, Double>> relatedFiles=Parser.AnswerQuestion(query, words);
		
		
//		relatedFiles.forEach(s->{
//			System.out.println(s.getKey()+"\t"+s.getValue());
//		});
		
		
		System.out.println("MOST SIMILAR FILE:\n"+relatedFiles.get(0));
	}

}
