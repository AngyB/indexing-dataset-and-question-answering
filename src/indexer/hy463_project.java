package indexer;


/**
 * 
 */
/**
 * @author Angela Braoudaki csd3275
 *
 */
import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import java.util.*;
import java.util.Map.Entry;

public class hy463_project {
	static String STOPWORDS_PATH = "./files/stopwords";
	static String COLLECTION_PATH = "./files/MiniCollection";

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		
		List<String>stopwords=new ArrayList<String>();
		Map<String,AnnotatedWord> words=new TreeMap<String,AnnotatedWord>();// the map that will contain the words and their frequencies
		//=====================Paths for testing====================
		String stoplistEnFilePath=STOPWORDS_PATH + "/stopwordsEn.txt";
		
		String stoplistGrFilePath=STOPWORDS_PATH + "/stopwordsGr.txt";
		//===============create stopwords list====================
		System.out.println("start creation of stoplists");
		
		stopwords=Parser.createStopwordsList(stoplistEnFilePath, stopwords);
		
		stopwords=Parser.createStopwordsList(stoplistGrFilePath, stopwords);
		//===============get all files from a folder==================
		System.out.println("get all paths of files");
		File folder = new File(COLLECTION_PATH);
		
		Map<String,AnnotatedDocument>filePaths=Parser.openFolder(folder);
		//===================parse all files =====================
		System.out.println("Parse all files");
		for ( Entry<String, AnnotatedDocument> entry : filePaths.entrySet()) {
//			System.out.println(entry.getKey());
			words=Parser.parse(entry.getValue(), stopwords, words);
		}
		
//		for ( Entry<String, AnnotatedDocument> entry : filePaths.entrySet()) {
//			System.out.println(entry.getKey()+"\t"+entry.getValue().getMaxWordFreq() +"\n");
//		}
		
		//=========writing in Files.txt==========
		Parser.calculateIdf(words,  filePaths);
		Parser.calculateNorm(words, filePaths);
		
		System.out.println("create Documents file");
		
		IndexCreator.createDocumentsFile("CollectionIndex", filePaths);
		//=================================================
		System.out.println("create Posting file");
		
		IndexCreator.createPostingFile("CollectionIndex", words, filePaths);
		//=================================================
		System.out.println("create Vocabulary file");
		
		IndexCreator.createVocabularyFile("CollectionIndex", words, filePaths);
		//=====================print variables===================
//		System.out.println(filePaths);
//		
//		System.out.println(stopwords);
		
//		for ( Entry<String, AnnotatedWord> entry : words.entrySet()) {
//			System.out.println(entry.getValue().toString()+"\n");
//		}
		
//		for ( Entry<String, AnnotatedDocument> entry : filePaths.entrySet()) {
//			System.out.println(entry.getKey()+"\t"+entry.getValue().getMaxWordFreq() +"\n");
//		}
		//==================================================
		System.out.println("finished");
	}


}