package indexer;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

abstract public class IndexCreator {
	
	public static Double log2(Double N){
  
        // calculate log2 N indirectly
        // using log() method
        Double result = (Math.log(N) / Math.log(2));
  
        return result;
    }
	
	public static void createVocabularyFile(String folderName, Map<String, AnnotatedWord>words, Map<String, AnnotatedDocument>filePaths) throws IOException{
		String fileName="VocabularyFile.txt";
		
//			creating the folder
		File directory = new File(folderName);
		if (! directory.exists()){
	        directory.mkdir();
	    }
		
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(folderName+"/"+fileName), StandardCharsets.UTF_8));
		
		String tmp;
		//Writing to the file
		for ( Entry<String, AnnotatedWord> entry : words.entrySet()) {
			
			//output format: word \t df \t idf \t pointer \n
			Integer df=entry.getValue().getFilesFrequency();
			Integer N=filePaths.size();
			Double idf=log2((double) (N/df));
			tmp=entry.getValue().getWord()+"\t"+df+"\t"+idf+"\t"+entry.getValue().getByteNo()+"\n";
			
			
			out.write(tmp);
			
		}
		
		out.close();
	}
	
	public static void createPostingFile(String folderName, Map<String, AnnotatedWord>words, Map<String, AnnotatedDocument>filePaths) throws IOException{
		String fileName="PostingFile.txt";
		
//		creating the folder
	File directory = new File(folderName);
	if (! directory.exists()){
        directory.mkdir();
    }
	
	BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(folderName+"/"+fileName), StandardCharsets.UTF_8));
	
	String tmp="";
	Integer byteNo=0;
	Double tf=0.0;
	List<String>tmpList;
	
	//Writing to the file
	for ( Entry<String, AnnotatedWord> entry : words.entrySet()) {
		
		entry.getValue().setByteNo(byteNo);
		tmpList=entry.getValue().getFileNames();
		
		for(int i=0;i<tmpList.size();i++) {
			
			//output format:  fileNo \t words tf \t pointer to documents file\n
			tf= (double)entry.getValue().getFileFrequency(tmpList.get(i)) / (double)filePaths.get(tmpList.get(i)).getMaxWordFreq();
//			tf=entry.getValue().getFileFrequency(tmpList.get(i));
			tmp=filePaths.get(tmpList.get(i)).getFileno()+"\t"+tf+"\t"+filePaths.get(tmpList.get(i)).getByteNo()+"\n";
			byteNo=byteNo+tmp.getBytes().length;
			
			out.write(tmp);
		}
		
	}
	
	out.close();
}
	
	
	public static void createDocumentsFile(String folderName,  Map<String, AnnotatedDocument>filePaths) throws IOException{
		
		String fileName= "DocumentsFile.txt";
		
//		creating the folder
		File directory = new File(folderName);
		if (! directory.exists()){
	        directory.mkdir();
	    }
		
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(folderName+"/"+fileName)));
		
		//Writing to the file
		Integer byteNo=0;
		String tmp;
		for ( Entry<String, AnnotatedDocument> entry : filePaths.entrySet()) {
			entry.getValue().setByteNo(byteNo);
			tmp=entry.getValue().getFileno()+"\t"+entry.getKey()+"\t"+entry.getValue().getNorm()+"\n";
			byteNo=byteNo+tmp.getBytes().length;
			out.write(tmp);
//			System.out.println("Start: "+entry.getValue().getStartByteNo()+" End: "+entry.getValue().getEndByteNo());
		}
		
		out.close();
	}
	
}
