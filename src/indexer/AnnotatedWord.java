package indexer;
import java.util.*;
import java.util.Map.Entry;


public class AnnotatedWord {
	private String word;
	private Integer freq;
	private Integer byteNo;
	private Double idf;
	private Map<String,Integer>labels;
	private Map<String,AnnotatedFile>files;
	
	public AnnotatedWord(String newWord,String newLabel, String newFile) {
		word=newWord;
		freq=1;
		setByteNo(0);
		labels=new TreeMap<String,Integer>();
		labels.put(newLabel,1);
		files=new TreeMap<String, AnnotatedFile>();
		AnnotatedFile file=new AnnotatedFile(newFile, newLabel);
		files.put(newFile,file);
	}
	
	public String  toString() {
		String str="";
		str=str+"WORD: "+word+"\n"+" WORD FREQUENCY: "+freq+"\n"+" LABELS:\n";
		//====================print labels=====================
		for ( Map.Entry<String, Integer> entry : labels.entrySet()) {
			str=str+"  "+entry.getKey()+":\n   LABEL FREQUENCY: "+entry.getValue()+"\n";
		}
		//=====================print files=====================
		str=str+" FILES:\n";
		for ( Map.Entry<String, AnnotatedFile> entry : files.entrySet()) {
			str=str+"  "+entry.getValue().toString()+"\n";
		}
		//================================================
		return str;
	}
	
	public String getWord() {
		return word;
	}
	
	public void addOccurance(String newLabel, String newFile, Integer newFileno) {
		freq++;
		//============new label=============
		if(labels.containsKey(newLabel)) {
			labels.put(newLabel, labels.get(newLabel)+1);
		}
		else {
			labels.put(newLabel,1);
		}
		//=============new file==============
		if(files.containsKey(newFile)) {
			files.get(newFile).addFreq();
			files.get(newFile).addLabel(newLabel);
		}
		else {
			AnnotatedFile file= new AnnotatedFile(newFile, newLabel);
			files.put(newFile, file);
		}
		//================================= 
	}
	
	Integer getFileFrequency(String filename) {
		return files.get(filename).getFreq();
	}
//	public Map<String, Integer> getFilesFrequency(Map<String, Integer>fileNorms) {
//		for ( Entry<String, AnnotatedFile> entry : files.entrySet()) {
//			if(fileNorms.containsKey(entry.getKey())) {
//				fileNorms.put(entry.getKey(), fileNorms.get(entry.getKey())+entry.getValue().getFreq());
//			}
//			else {
//				fileNorms.put(entry.getKey(), entry.getValue().getFreq());
//			}
//		}
//		return fileNorms;
//	}
	
	public Integer getFilesFrequency() {
		return files.size();
	}
	
	public List<String> getFileNames(){
		List<String>filenames=new ArrayList<String>();
		for ( Entry<String, AnnotatedFile> entry : files.entrySet()) {
			filenames.add(entry.getKey());
		}
		
		return filenames;
	}
	
	public Integer getByteNo() {
		return byteNo;
	}

	public void setByteNo(Integer newByteNo) {
		byteNo = newByteNo;
	}
	public Double getIdf() {
		return idf;
	}

	public void setIdf(Double idf) {
		this.idf = idf;
	}
	//	T=T=T=T=T=T=T=T=T=T=T=T=T INNER CLASS=T=T=T=T=T=T=T=T=T=T
	private class AnnotatedFile {
		private String file;
		private Integer freq;
		private Map<String,Integer>labels;
		public AnnotatedFile(String fileName, String newLabel) {
			file=fileName;
			freq=1;
			labels=new TreeMap<String,Integer>();
			labels.put(newLabel,1);
		}
		
		public void addFreq() {
			freq++;
		}
		
		public Integer getFreq() {
			return freq;
		}
		public String toString() {
			String str="";
			str=str+"FILE: "+file+"\n   FILE FREQUENCY: "+freq+"\n"+"   LABELS:\n";
			for ( Map.Entry<String, Integer> entry : labels.entrySet()) {
				str=str+"    "+entry.getKey()+":\n     LABEL FREQUENCY: "+entry.getValue()+"\n";
			}
			return str;
		}
		
		public void addLabel(String newLabel) {
			if(labels.containsKey(newLabel)) {
				labels.put(newLabel, labels.get(newLabel)+1);
			}
			else {
				labels.put(newLabel,1);
			}
		}
		
		
	}
//	T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T=T
}
