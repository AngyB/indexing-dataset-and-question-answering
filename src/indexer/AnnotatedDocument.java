package indexer;


public class AnnotatedDocument {
	private String fileName;
	private Integer fileno;
	private Double norm;
	private Integer maxWordFreq;
	private Integer byteNo;
	
	public AnnotatedDocument(String newFileName, Integer newFileno) {
		fileName=newFileName;
		fileno=newFileno;
		norm=0.0;
		maxWordFreq=0;
		byteNo=0;
	}

	public void setMaxWordFreq(Integer newMax) {
		maxWordFreq=newMax;
	}
	
	public Integer getMaxWordFreq() {
		return maxWordFreq;
	}
	
	public void setNorm(Double newNorm) {
		 norm=newNorm;
	}
	
	public void addNorm(Double newNorm) {
		 norm=norm+newNorm;
	}
	
	public Double getNorm() {
		return  norm;
	}
	
	public Integer getFileno() {
		return fileno;
	}
	
	public String getName() {
		return fileName;
	}
	
	public void setByteNo(Integer newByteNo) {
		byteNo=newByteNo;
	}
	
	
	public Integer getByteNo() {
		return byteNo;
	}
	
	
	
}
