package indexer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import opennlp.tools.stemmer.PorterStemmer;
//import opennlp.tools.stemmer.Stemmer;

abstract public class Parser {
	static Integer fileNo=0;
	//TOKENIZATION STEMMING
	
	private static List<String> tokenization(String sentance, List<String>stopwords, AnnotatedDocument file, Map<String,Integer>wordsfreq) {
		String delimiter = "\s\t\n\r\f. , ? # & ( ) ; ~ { }+ - / \\  | = : !\" \' $ % _ * [ ] @ ^ ` 0 1 2 3 4 5 6 7 8 9";
		StringTokenizer tokenizer = new StringTokenizer(sentance, delimiter);
		List<String> words=new ArrayList<String>();
		Integer maxWordFreq=0;
		
		PorterStemmer stemmer= new PorterStemmer();
		//		Stemmer.Initialize();
		while(tokenizer.hasMoreTokens()) {
			String currentToken = tokenizer.nextToken();
//			currentToken=Stemmer.Stem(currentToken);
			
			currentToken=stemmer.stem(currentToken);
			
			if(!stopwords.contains(currentToken)&&currentToken.length()>2) {
				words.add(currentToken);
				//==========calculate max frequency in file=========
				if(wordsfreq.containsKey(currentToken)) {
					wordsfreq.put(currentToken, wordsfreq.get(currentToken)+1);
				}
				else {
					wordsfreq.put(currentToken, 1);
				}
				
				if(wordsfreq.get(currentToken)>maxWordFreq) {
					maxWordFreq=wordsfreq.get(currentToken);
				}
			}
			//=======================================
		}
		
		if(file.getMaxWordFreq() < maxWordFreq) {
			file.setMaxWordFreq(maxWordFreq);
		}
		return words;
	}
	
	
	//CREATE LIST WITH STOPWORDS
	public static List<String> createStopwordsList(String filepath, List <String> stopwords) throws IOException{
		
		String line="";
		FileReader file = new FileReader(filepath);
		
		@SuppressWarnings("resource")
		BufferedReader reader= new BufferedReader(file);
		while(line!=null) {
			line=reader.readLine();
			if(line!=null) {
				line.replaceAll("\\s","");
				stopwords.add(line);
			}
		}
		
		return stopwords;
	}
	
	
	//CREATE MAP WITH WORDS AND FREQUENCIES
	private static Map<String,AnnotatedWord> createTokenMap(List<String>tokens,Map<String,AnnotatedWord> words, String label, AnnotatedDocument file){
		
//		file.addNorm(tokens.size());
		
		for(int i=0;i<tokens.size();i++) {
			   
			   if(words.containsKey(tokens.get(i))) {
					words.get(tokens.get(i)).addOccurance(label, file.getName(), file.getFileno());
				}
			   else {
				   AnnotatedWord tmpWord=new AnnotatedWord(tokens.get(i), label, file.getName());
				   words.put(tokens.get(i), tmpWord);
			   }
		
		   }
		return words;
	}
	
	
	
	public static Map<String,AnnotatedDocument> openFolder(File folder){
		Map <String,AnnotatedDocument>filePaths= new TreeMap<String,AnnotatedDocument>();
		
		
		if(folder.listFiles()==null) {
			AnnotatedDocument newdoc=new AnnotatedDocument(folder.toString(),fileNo);
			filePaths.put(folder.toString(),newdoc);
			fileNo++;
			return filePaths;
		}
		
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				Map<String,AnnotatedDocument>tmp=openFolder(fileEntry);
				
				for ( Entry<String,AnnotatedDocument> entry : tmp.entrySet()) {
					AnnotatedDocument newdoc=new AnnotatedDocument(entry.getKey(), fileNo);
					filePaths.put(entry.getKey(),newdoc);
					fileNo++;
				}
			} 
			else {
				AnnotatedDocument newdoc=new AnnotatedDocument(fileEntry.getPath(), fileNo);
				filePaths.put(fileEntry.getPath(),newdoc);
				fileNo++;
//				System.out.println(fileEntry.getAbsolutePath());
			}
		}
		return filePaths;
	}
	
	
	
	@SuppressWarnings("null")
	public static Map<String, AnnotatedWord>parse(AnnotatedDocument filePath, List<String> stopwords, Map<String,AnnotatedWord>words) throws UnsupportedEncodingException, IOException, SAXException, ParserConfigurationException{
		String strAuthor="";
		String strCategories="";
		File example = new File(filePath.getName());
		
//		NXMLFileReader xmlFile = new NXMLFileReader(example);
		Map<String,Integer>wordsfreq=new TreeMap<String, Integer>();
//		
//		ArrayList<String> authors = xmlFile.getAuthors();
//		HashSet<String> categories =xmlFile.getCategories();
//		
//		
//		for (int i = 0; i < authors.size(); i++) {
//            strAuthor=strAuthor+authors.get(i)+" ";
//        }
//		
//		System.out.println(strAuthor);
//		
//		for (String value : categories) {
//			strCategories=strCategories+value+" ";
//	    }
//		
//		words=createTokenMap(tokenization(xmlFile.getPMCID(), stopwords, filePath, wordsfreq), words,"PMC ID",filePath);
//		words=createTokenMap(tokenization(xmlFile.getTitle(), stopwords, filePath, wordsfreq), words,"Title", filePath);
//		words=createTokenMap(tokenization(xmlFile.getAbstr(), stopwords, filePath, wordsfreq), words,"Abstract", filePath);
//		words=createTokenMap(tokenization(xmlFile.getBody(), stopwords, filePath, wordsfreq), words,"Body", filePath);
//		words=createTokenMap(tokenization(xmlFile.getJournal(), stopwords, filePath, wordsfreq), words,"Journal", filePath);
//		words=createTokenMap(tokenization(xmlFile.getPublisher(), stopwords, filePath, wordsfreq), words,"Publisher", filePath);
//		words=createTokenMap(tokenization(strAuthor, stopwords, filePath, wordsfreq), words,"Authors", filePath);
//		words=createTokenMap(tokenization(strCategories, stopwords, filePath, wordsfreq), words,"Categories", filePath);
//		
//		=============================================
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
		dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
		
		 DocumentBuilder db = dbf.newDocumentBuilder();

         Document doc = db.parse(example);

        
//         System.out.println("------");

         // get <staff>
         NodeList authors = doc.getElementsByTagName("name");
         
         for (int i = 0; i < authors.getLength(); i++) {
        	 strAuthor=strAuthor+" "+authors.item(i).getTextContent();
         }
         
         
         NodeList categories = doc.getElementsByTagName("article-categories");
         
         for (int i = 0; i < categories.getLength(); i++) {
        	 strCategories=strCategories+" "+categories.item(i).getTextContent();
         }
         
         
         NodeList pmcid = doc.getElementsByTagName("article-id");
         
         NodeList title = doc.getElementsByTagName("article-title");
         
         NodeList abst = doc.getElementsByTagName("abstract");
         
         NodeList body = doc.getElementsByTagName("body");
         
         NodeList journal = doc.getElementsByTagName("journal-meta");
         
         NodeList publisher = doc.getElementsByTagName("publisher-name");
         
//         System.out.println(journal.item(0).getTextContent());
         
		words=createTokenMap(tokenization(pmcid.item(0).getTextContent(), stopwords, filePath, wordsfreq), words,"PMC ID",filePath);
		words=createTokenMap(tokenization(title.item(0).getTextContent(), stopwords, filePath, wordsfreq), words,"Title", filePath);
 		words=createTokenMap(tokenization(abst.item(0).getTextContent(), stopwords, filePath, wordsfreq), words,"Abstract", filePath);
 		words=createTokenMap(tokenization(body.item(0).getTextContent(), stopwords, filePath, wordsfreq), words,"Body", filePath);
 		words=createTokenMap(tokenization(journal.item(0).getTextContent(), stopwords, filePath, wordsfreq), words,"Journal", filePath);
 		words=createTokenMap(tokenization(publisher.item(0).getTextContent(), stopwords, filePath, wordsfreq), words,"Publisher", filePath);
 		words=createTokenMap(tokenization(strAuthor, stopwords, filePath, wordsfreq), words,"Authors", filePath);
 		words=createTokenMap(tokenization(strCategories, stopwords, filePath, wordsfreq), words,"Categories", filePath);

		return words;
				
	}
	
	public static Double log2(Double N){
		  
        // calculate log2 N indirectly
        // using log() method
        Double result = (Math.log(N) / Math.log(2));
  
        return result;
    }
	
	public static void calculateIdf(Map <String, AnnotatedWord>words, Map <String, AnnotatedDocument> filePaths){
		for ( Entry<String, AnnotatedWord> entry : words.entrySet()) {
			
			Integer df=entry.getValue().getFilesFrequency();
			Integer N=filePaths.size();
			Double idf=log2((double) (N/df));
			
			entry.getValue().setIdf(idf);
		}
		
	}
	
	public static void calculateNorm(Map <String, AnnotatedWord>words, Map <String, AnnotatedDocument> filePaths) {
		
		
		Double tf=0.0;
		List<String>tmpList;
		
		for ( Entry<String, AnnotatedWord> entry : words.entrySet()) {
			
			tmpList=entry.getValue().getFileNames();
			
			for(int i=0;i<tmpList.size();i++) {
				
				
				tf= (double)entry.getValue().getFileFrequency(tmpList.get(i)) / (double)filePaths.get(tmpList.get(i)).getMaxWordFreq();
//				tf=entry.getValue().getFileFrequency(tmpList.get(i));
				
				
				Double w=entry.getValue().getIdf()*tf;
				
				filePaths.get(tmpList.get(i)).addNorm(w*w);
				
//				tmp=filePaths.get(tmpList.get(i)).getFileno()+"\t"+tf+"\t"+filePaths.get(tmpList.get(i)).getByteNo()+"\n";
			
				
			}
			
		}
		for ( Entry<String, AnnotatedDocument> entry : filePaths.entrySet()) {
			entry.getValue().setNorm(Math.sqrt(entry.getValue().getNorm()));
		}
		
	}
	
}
