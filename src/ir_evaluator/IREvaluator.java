package ir_evaluator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

//import gr.uoc.csd.hy463.Topic;
//import gr.uoc.csd.hy463.TopicsReader;
import query_eval.AnnotatedWord;
import query_eval.Parser;

public class IREvaluator {

	static String TOPICS_PATH = "./files/topics.xml";
	static String QRELS_PATH = "./files/qrels.txt";
	static String RESULTS_FILENAME = "results.txt";

	public static ArrayList<List<Entry<String, Double>>> evaluateTopics() throws Exception {
		Map<String, AnnotatedWord> vocabulary = new HashMap<String, AnnotatedWord>();
		ArrayList<Topic> topics = TopicsReader.readTopics(TOPICS_PATH);
		ArrayList<List<Entry<String, Double>>> queryResults = new ArrayList<List<Entry<String, Double>>>();
		Parser.Init();
		Parser.getVocabulary(vocabulary);
		//============calculate related documents==============
		for (int i = 0; i < topics.size(); ++i) {
			// Use the topic summary as the query string since it produces better results
			System.out.println("Topic " + (i + 1));
			
			String topic_query = topics.get(i).getSummary();
			List<Entry<String, Double>> queryResult = null;
		
			queryResult = Parser.AnswerQuestion(topic_query, vocabulary);
			
			queryResults.add(queryResult);
		}
		
		return queryResults;
	}

	public static void writeTopicResultsToFile(String filename, ArrayList<List<Entry<String, Double>>> topicQueryResults) throws Exception {
		ArrayList<Topic> topics = TopicsReader.readTopics(TOPICS_PATH);
		BufferedWriter results = new BufferedWriter(new FileWriter(filename));
		
		for (int i = 0; i < topics.size(); ++i) {
			List<Entry<String, Double>> queryResult = topicQueryResults.get(i);
			
			for (int rank = 0; rank < 1000; ++rank) {
				Entry<String, Double> ev;
				try {
					ev = queryResult.get(rank);
				}
				catch(Exception e){
					results.close();
					return;
				}
				
				if (ev.getValue() == 0) {
					break;
				}
				//======get pmc id=======
				String[] tokens = ev.getKey().split("\\.");
				tokens = tokens[tokens.length - 2].split("/");
				String pmcid = tokens[tokens.length - 1];
				
				results.write((i + 1) + "\t0\t" + pmcid + "\t" + (rank + 1) + "\t" + ev.getValue() + "\n");
			}
		}
		results.close();
	}
	
	public static ArrayList<HashMap<Integer, Integer>> readQueryTrueResults(String filename) throws IOException {
		// The array contains a hashmap with (pmcid, relevance) pairs read from the qrels file for each topic
		ArrayList<HashMap<Integer, Integer>> trueResults = new ArrayList<HashMap<Integer, Integer>>();
		BufferedReader qrels = new BufferedReader(new FileReader(filename));

		String line = qrels.readLine();
		int currentTopic = 1;
		
		HashMap<Integer, Integer> currentTopicResults = new HashMap<Integer, Integer>();
		
		while (line != null) {
			String[] tokens = line.split("\t");
			
			int topicNo = Integer.parseInt(tokens[0]);
			Integer pmcid = Integer.parseInt(tokens[2]);
			Integer relevance = Integer.parseInt(tokens[3]);
			
			// ===store topic in TrueResults create new topic======
			if (topicNo != currentTopic) {
				trueResults.add(currentTopicResults);
				currentTopicResults = new HashMap<Integer, Integer>();
				currentTopic = topicNo;
			}
			//=========save fields of results========
			currentTopicResults.put(pmcid, relevance);
			line = qrels.readLine();
		}
		
		trueResults.add(currentTopicResults);

		qrels.close();
		return trueResults;
	}
	
	public static ArrayList<List<QueryResult>> readResultsFromFile(String filename) throws IOException {
		
		ArrayList<List<QueryResult>> topicQueryResults = new ArrayList<>();
		BufferedReader results = new BufferedReader(new FileReader(filename));

		String line = results.readLine();
		int currentTopic = 1;
		ArrayList<QueryResult> currentTopicResults = new ArrayList<>();
		
		while (line != null) {
			String[] tokens = line.split("\t");
			
			int topicNo = Integer.parseInt(tokens[0]);
			int pmcid = Integer.parseInt(tokens[2]);
			double score = Double.parseDouble(tokens[4]);
			
			if (topicNo != currentTopic) {
				topicQueryResults.add(currentTopicResults);
				currentTopicResults = new ArrayList<>();
				currentTopic = topicNo;
			}
			
			currentTopicResults.add(new QueryResult(pmcid, score));
			line = results.readLine();
		}
		
		topicQueryResults.add(currentTopicResults);

		results.close();
		return topicQueryResults;
	}
	

	public static void main(String[] args) {
		
		ArrayList<HashMap<Integer, Integer>> topicTrueResults = null;
		ArrayList<List<QueryResult>> topicResults = null;

		System.out.println("Reading results");
		//===========try to open results filename========
		try {
			// Load the results of each topic query
			topicResults = readResultsFromFile(RESULTS_FILENAME);
		} 
		catch (IOException e) {
			// =====Create the results file since it doesn't exist====
			System.out.println("Results file " + RESULTS_FILENAME + " not found. Generating...");
			ArrayList<List<Entry<String, Double>>> topicQueryResults = null;
			//============try to read TOPICS_PATH=========
			try {
				topicQueryResults = evaluateTopics();
			} 
			catch (Exception e1) {
				System.err.println("Could not evaluate topics");
				e1.printStackTrace();
				return;
			}
			//=========try to write in RESULTS_FILENAME=======
			try {
				writeTopicResultsToFile(RESULTS_FILENAME, topicQueryResults);
			} 
			catch (Exception e1) {
				System.err.println("Could not write topic evaluation results to file " + RESULTS_FILENAME);
				e1.printStackTrace();
				return;
			}
			//=====================================
			
			System.out.println("Created results file " + RESULTS_FILENAME);
			
			//===now that you have created Results file read it===== 
			try {
				topicResults = readResultsFromFile(RESULTS_FILENAME);
			} 
			catch (IOException e1) {
				System.err.println("Could not read results file " + RESULTS_FILENAME);
				e1.printStackTrace();
			}
			//=====================================
		}

		System.out.println("Reading true query results");
		
		//==============read the true results============
		try {
			topicTrueResults = readQueryTrueResults(QRELS_PATH);
		} catch (IOException e) {
			System.err.println("Could not read true topic results from file " + QRELS_PATH);
			e.printStackTrace();
			return;
		}

		//========Evaluate IRS results for each topic========
		ArrayList<TopicResultsEvaluation> evaluationResults = new ArrayList<>();
		for (int topic = 1; topic <= 30; ++topic) {
			int index = topic - 1;
			System.out.println("Evaluating results for topic " + topic);
			if(topicResults.size()>index) {
				List<QueryResult> result = topicResults.get(index);
				HashMap<Integer, Integer> trueResult = topicTrueResults.get(index);
				
				TopicResultsEvaluation resultEval = new TopicResultsEvaluation(topic);
				resultEval.evaluate(result, trueResult);
				evaluationResults.add(resultEval);
			}
		}
		//=====================================
		for (TopicResultsEvaluation tre : evaluationResults) {
			System.out.println(tre);
		}
		
	}
	
}
