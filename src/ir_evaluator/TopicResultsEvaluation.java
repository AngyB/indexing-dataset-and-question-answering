package ir_evaluator;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class TopicResultsEvaluation {
	int topicNo;
	double bpref;
	double ndcg;
	double avep;

	TopicResultsEvaluation(int topicNo) {
		this.topicNo = topicNo;
	}
	
	public static double log2(double x) {
		return Math.log(x) / Math.log(2);
	}

	public void evaluate(List<QueryResult> queryResults, HashMap<Integer, Integer> trueResults) {
		int R = 0; // total docs judged relevant
		int R1 = 0; // total docs with relevance == 1
		int R2 = 0; // total docs with relevance == 2 
		
		double N = 0; // total judged irrelevant
		int nonRelevant = 0; // running count of non relevant docs in query result
		int relevant = 0; // running count of relevant docs in query result
		
		bpref = 0;
		ndcg = 0;
		avep = 0;
		
		double idcg = 0;
		double dcg = 0;
		double precision;
		
		//===calculate R (total number of relevant documents)===
		for (Entry<Integer, Integer> e : trueResults.entrySet()) {
			if (e.getValue() == 1)
				++R1;
			else if (e.getValue() == 2)
				++R2;
			else
				++N;
		}
		
		R = R1 + R2;
		
		for (int i = 0; i < queryResults.size(); ++i) {
			QueryResult res = queryResults.get(i);
			Integer relevance = trueResults.get(res.pmcid);
			// Skip documents with no judgment
			if (relevance == null)
				continue;
			//============bpref============
			if (relevance == 0) {
				++nonRelevant;
			}
			else {
				bpref += 1 - nonRelevant / Math.min(R, N);
				++relevant;
			}
			//============ndcg============
			int idcg_relevance = 0;
			
			if (R2-- > 0) {
				idcg_relevance = 2;
			}
			else if (R1-- > 0) {
				idcg_relevance = 1;
			}
			
			if (i == 0) {
				dcg += relevance;
				idcg += idcg_relevance;
			} 
			else {
				dcg += relevance / log2(i + 1);
				idcg += idcg_relevance / log2(i + 1);
			}
			
			//===========avep=============
			precision =  relevant / (i + 1);
			avep += precision * relevance;
		}
		
		System.out.println("Topic " + topicNo + " has " + relevant + " relevant docs in query result out of " + R);
		bpref = bpref/R;
		
		if (bpref < 0) {
			bpref = 0;
		}
		
		ndcg = dcg/idcg;
		avep = avep/R;
		
	}

	@Override
	public String toString() {
		return topicNo + "\t" + bpref + "\t" + avep + "\t" + ndcg;
	}
}
