package ir_evaluator;

public class QueryResult {
	int pmcid;
	double score;
	
	QueryResult(int pmcid, double score) {
		this.pmcid = pmcid;
		this.score = score;
	}
}
